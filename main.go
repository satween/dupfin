package main

import (
	"crypto/sha1"
	"encoding/csv"
	"encoding/hex"
	"fmt"
	"github.com/dustin/go-humanize"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"time"
)

type Duplicate struct {
	paths []string
	size  uint64
}

func (d Duplicate) Overhead() uint64 { return d.size * uint64(len(d.paths)-1) }

const DumpFileName = "dupfin_dump"
const DumpResultsFileName = "dupfin_results"
const DumpFileType = ".csv"
const VERSION = "1.4"

type DuplicateList []Duplicate

func (p DuplicateList) Len() int      { return len(p) }
func (p DuplicateList) Swap(i, j int) { p[i], p[j] = p[j], p[i] }
func (p DuplicateList) Less(i, j int) bool {
	return p[i].size*uint64(len(p[i].paths)) < p[j].size*uint64(len(p[j].paths))
}

var duplicates = make(map[string]Duplicate)

func visit(path string, f os.FileInfo, err error) error {

	if !f.IsDir() {
		bytes, _ := ioutil.ReadFile(path)
		checksum := sha1.Sum(bytes)
		checksumStr := hex.EncodeToString(checksum[:])
		size := uint64(len(bytes))
		fmt.Printf("Visiting: (%s) %s \n", humanize.Bytes(size), path)
		duplicate, hasList := duplicates[checksumStr]
		if !hasList {
			duplicates[checksumStr] = Duplicate{paths: []string{path}, size: size}
		} else {
			duplicate.paths = append(duplicate.paths, path)
			duplicates[checksumStr] = duplicate
		}
	}

	return nil
}

func dump(w io.Writer) {
	writer := csv.NewWriter(w)
	defer writer.Flush()
	for key, duplication := range duplicates {
		if len(duplication.paths) > 1 {
			data := []string{key, strconv.FormatUint(duplication.size, 10)}
			for _, path := range duplication.paths {
				data = append(data, string(path))
			}
			writer.Write(data)
		}
	}
}

func dumpResults(w io.Writer, dupList DuplicateList) {
	writer := csv.NewWriter(w)
	defer writer.Flush()
	for _, duplication := range dupList {
		paths := duplication.paths

		if len(paths) > 1 {
			data := []string{strconv.FormatUint(duplication.Overhead(), 10)}
			for _, path := range paths {
				data = append(data, path)
			}
			writer.Write(data)
		}
	}
}

func createResultsFile(fileName string) *os.File {
	index := 0
	dumpFileName := fileName + DumpFileType
	for isExist(dumpFileName) {
		index++
		dumpFileName = fileName + "(" + strconv.Itoa(index) + ")" + DumpFileType
	}

	writer, err := os.Create(dumpFileName)
	if err != nil {
		fmt.Printf("Cannot create dump file %s %v\n", dumpFileName, err)
		return nil
	}
	return writer
}

func isExist(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func printUsage() (int, error) {
	return fmt.Printf("Usage:\n\n%s [options] <path to root directory>\n\nOptions:\n\t -d dumps work state into a file\n\t -r dumps work results into a file\n\t -v prints current version\n\t -h prints this help and exit\n", os.Args[0])
}

func main() {
	var dumpCsvWriter *os.File = nil
	var dumpResultsCsvWriter *os.File = nil

	if len(os.Args) < 2 {
		printUsage()
		return
	}
	var root *string = nil
	for i, v := range os.Args {
		if v == "-h" {
			printUsage()
			return
		} else if v == "-v" {
			fmt.Printf("Version: %s \n", VERSION)
			if len(os.Args) == 2 {
				return
			}
		} else if v == "-d" {
			dumpCsvWriter = createResultsFile(DumpFileName)
			break
		} else if v == "-r" {
			dumpResultsCsvWriter = createResultsFile(DumpResultsFileName)
			break
		} else if i > 0 {
			root = &os.Args[i]
			info, err := os.Stat(*root)
			if os.IsNotExist(err) {
				fmt.Printf("Path \"%s\" does not exist\n", *root)
				return
			} else if err != nil {
				fmt.Printf("Unknown error with %s : %v\n", *root, err)
				return
			} else if !info.IsDir() {
				fmt.Printf("Path \"%s\" is not a directory\n", *root)
				return
			}
		}
	}

	if root == nil {
		printUsage()
		return
	}

	start := time.Now()
	filepath.Walk(*root, visit)

	dupList := make(DuplicateList, len(duplicates))
	i := 0
	for _, d := range duplicates {
		dupList[i] = d
		i++
	}

	sort.Sort(dupList)

	fmt.Println()
	for _, duplication := range dupList {
		paths := duplication.paths
		if len(paths) > 1 {
			fmt.Printf("Duplicates found: (%s) extra\n", humanize.Bytes(duplication.Overhead()))
			for _, path := range paths {
				fmt.Printf("%s  \n", path)
			}
			fmt.Println()
		}
	}

	if dumpCsvWriter != nil {
		dump(dumpCsvWriter)
	}

	if dumpResultsCsvWriter != nil {
		dumpResults(dumpResultsCsvWriter, dupList)
	}

	fmt.Printf("Completed! Took: %v\n", time.Since(start))
}
